#!/bin/sh

YESTERDAY=`date -d yesterday +%Y-%m-%d`
grep "^$YESTERDAY" services.log | gzip > services.log.$YESTERDAY.gz
sed -i "/^$YESTERDAY/d" services.log
