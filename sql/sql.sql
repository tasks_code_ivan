/*
 * I'm using sqlite. I'm assuming data is in "table1"...
 *
 * Schema of table1 is...
 * CREATE TABLE table1(user_id smallint, step_performed smallint, last_step smallint);
 */
SELECT user_id, last_step-performed AS missing_steps
FROM
(SELECT user_id, last_step, count(user_id) AS performed
FROM table1
GROUP BY user_id)
WHERE 0 <> last_step-performed
GROUP BY user_id;
