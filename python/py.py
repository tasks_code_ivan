#!/usr/bin/env python

"""Find bouncing addresses in mbox, and write them to a file"""

class BouncingAddresses(object):
    """Find bouncing addresses in mbox, and write them to a file.

    Our two methods roughly correspond to the finding, and to the writing.
    """
    def __init__(self, mbox, path):
        """We take as arguments two paths...

        + Path of mbox

        + Path of file addresses will be stored in
        """
        self.mbox = mbox
        self.a_path = path
        self.bouncing_addresses = set()

    def find_addresses(self):
        """Find addresses in mbox.

        message.walk() method of mailbox module iterates through a message's
        parts.

        We think that bouncing addresses are found in the last line of each
        message's 2nd part.
        """
        for message in mailbox.mbox(self.mbox):
            i = 0
            for part in message.walk():
                # Perhaps it suffices to look at part==1 of message?
                if i == 1:
                    an_error_occurred_etc = part.as_string()
                    address = an_error_occurred_etc.split('\n').pop()
                    if re.match("[^@]+@[^@]+\.[^@]+", address):
                        self.bouncing_addresses.add(address)
                    break
                i += 1

    def store_addresses_in_file(self):
        """Write found addresses to file specified in arguments."""
        my_file = open(self.a_path, 'w')
        for address in self.bouncing_addresses:
            my_file.write(address + '\n')
        my_file.close()

if __name__ == "__main__":

    import mailbox
    import re

    bouncing_addresses = BouncingAddresses('/home/me/emails.mbox',\
            '/home/me/bouncing_addresses')
    bouncing_addresses.find_addresses()
    bouncing_addresses.store_addresses_in_file()
